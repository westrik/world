import { h } from 'preact';

import AppContainer from '~components/AppContainer';

export default function Dashboard(): h.JSX.Element {
    return (
        <AppContainer>
            <span>dashboard goes here</span>
        </AppContainer>
    );
}
