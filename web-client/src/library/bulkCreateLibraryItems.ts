import { AuthContext } from '~auth/AuthContext';
import { FileType } from '~components/FileUploadField';
import { ApiLibraryItem, ApiLibraryItemVersion } from '~models/LibraryItem';
import { assertCondition } from '~utils/asserts';
import { ApiResponse, request, RequestMethod } from '~utils/network';

interface BulkCreateLibraryItemsRequest {
    fileSpecs: Array<[number, FileType]>;
}

export interface BulkCreateLibraryItemsResponse extends ApiResponse {
    libraryItems: Array<ApiLibraryItem>;
}

interface CreateLibraryItemVersionRequest {
    libraryItemId: string;
}

export interface CreateLibraryItemVersionResponse extends ApiResponse {
    libraryItemVersion: ApiLibraryItemVersion;
}

async function createLibraryItem(authContext: AuthContext, item: ApiLibraryItem, file: File): Promise<void> {
    assertCondition(
        file && file.size == item.uploadedFileSizeBytes,
        'Expected size of created library item to match file size',
    );
    // TODO: improve error-handling
    const uploadResponse = await fetch(item.preSignedUploadUrl, {
        body: file,
        method: RequestMethod.PUT,
    });
    if (uploadResponse.ok) {
        const createVersionResponse = await request<CreateLibraryItemVersionRequest, CreateLibraryItemVersionResponse>(
            RequestMethod.POST,
            '/library-item-version',
            authContext,
            {
                libraryItemId: item.id,
            },
        );
        console.log(createVersionResponse);
    } else {
        // TODO: retry upload
    }
}

enum UploadStatus {
    COMPLETE,
    UPLOADING,
}

interface UploadState {
    status: UploadStatus;
    completedFiles?: number;
    totalFiles?: number;
    completedBytes?: number;
    totalBytes?: number;
}

export default async function bulkCreateLibraryItems(
    authContext: AuthContext,
    files: Array<File>,
    onStatusChange: (status: UploadState) => void,
): Promise<void> {
    const response = await request<BulkCreateLibraryItemsRequest, BulkCreateLibraryItemsResponse>(
        RequestMethod.POST,
        `/library-item:bulk-create`,
        authContext,
        {
            fileSpecs: files.map((file) => [file.size, file.type as FileType]),
        },
    );
    onStatusChange({ status: UploadStatus.COMPLETE });
    // TODO: improve error-handling
    if (response) {
        const sortedLibraryItems = response.libraryItems.sort(
            (a, b) => a.uploadedFileSizeBytes - b.uploadedFileSizeBytes,
        );
        const sortedFiles = files.sort((a, b) => a.size - b.size); // TODO: rate-limit & batch item creation // TODO: track progress
        sortedLibraryItems.map((item, idx) => createLibraryItem(authContext, item, sortedFiles[idx]));
    }
}
